class Api::V1::TypesController < Api::V1::BaseController
  before_action :authenticate_user!
  before_action :set_type, except: [:index, :create]

  def index
    @types = Type.all
  end

  def create
    @type.save
  end

  def update
    @type.update(type_params)
  end

  def show
  end

  def destroy
    @type.destroy
  end

  private
  def set_type
    @type = Type.find(params[:id])
  end

  def type_params
    params.permit(:name)
  end   
end
