class Api::V1::GroupsController < Api::V1::BaseController
  before_action :authenticate_user!, except: [:index]
  before_action :set_group, except: [:index, :create]

  def index
    params[:page] ||= 1
    if params.has_key? :name and params[:name] != ""
      @groups = current_user.groups.page(params[:page])
    else
      @groups = Group.all.page(params[:page])
    end
  end

  def create
    @group.save
  end

  def update
    @group.update(group_params)
  end

  def show
  end

  def destroy
    @group.destroy
  end

  private
  def set_group
    @group = Group.find(params[:id])
  end

  def group_params
    params.permit(:name, :members)
  end
end
