class Api::V1::ReviewsController < Api::V1::BaseController
  before_action :authenticate_user!
  before_action :set_review, except: [:index, :create]

  def index
    @reviews = Review.all
  end

  def create
    @review.save
  end

  def update
    @review.update(event_params)
  end

  def show
  end

  def destroy
    @review.destroy
  end

  private
  def set_event
    @review = Review.find(params[:id])
  end

  def event_params
    params.permit(:title, :start, :end)
  end
end
