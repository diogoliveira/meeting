class Api::V1::CancellationsController < Api::V1::BaseController
  before_action :authenticate_user!

  def index
    @cancellation = current_user.cancellations
  end

  def create
    @cancellation.save
  end

  private
  def cancellation_params
    parmas.permit(:reason)
  end
end
