class Api::V1::EventsController < Api::V1::BaseController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_event, except: [:index, :create]

  def index
    @events = Event.all
  end

  def create
    @event.save
  end

  def update
    @event.update(event_params)
  end

  def show
  end

  def destroy
    @event.destroy
  end

  private
  def set_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.permit(:title, :start, :end)
  end
end
