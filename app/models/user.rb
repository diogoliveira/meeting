class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :my_events, :class_name => :Event, :foreign_key => "owner"
  has_many :cancellations
  has_many :reviews
  has_many :user_types
  has_many :types, through: :user_types
  has_many :user_events
  has_many :events, through: :user_events
  has_many :user_groups
  has_many :groups, through: :user_groups

  enum role: [:user, :admin]

  include Authenticatable

end
