class Event < ApplicationRecord
  belongs_to :owner, :class_name => :User,:foreign_key => "owner"
  belongs_to :cancellation
  belongs_to :type

  has_many :reviews
  has_many :user_events
  has_many :users, through: :user_events
end
