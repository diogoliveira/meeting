class CreateReviews < ActiveRecord::Migration[5.1]
  def change
    create_table :reviews do |t|
      t.references :user,   foreing_key:true, indedx:false
      t.references :event,  foreing_key:true, indedx:false
      t.string :message,    null: false, default: ""

      t.timestamps
    end
  end
end
