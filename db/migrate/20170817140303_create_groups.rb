class CreateGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :groups do |t|
      t.string :name,     null: false, default: ""
      t.integer :members, null: false, default: 0

      t.timestamps
    end
  end
end
