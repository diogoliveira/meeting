class CreateUserEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :user_events do |t|
      t.references :user, foreign_key: true, index: false
      t.references :event, foreign_key: true, index: false

      t.timestamps
    end
  end
end
