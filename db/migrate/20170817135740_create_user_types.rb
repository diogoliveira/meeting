class CreateUserTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :user_types do |t|
      t.references :user, foreing_key:true, indedx:false
      t.references :type, foreing_key:true, indedx:false

      t.timestamps
    end
  end
end
