class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.references :type, foreing_key: true, indedx: false
      t.references :owner
      t.string :title, null: false, default: ""
      t.datetime :start, null: false
      t.datetime :end, null: false

      t.timestamps
    end
  end
end
